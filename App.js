/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import { Navigator, StatusBar, Platform, StyleSheet, Text, View, Alert, AsyncStorage } from 'react-native'
// import { WebView } from 'react-native-webview';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator, Header } from 'react-navigation-stack';
// import FlashMessage, { showMessage, hideMessage } from "react-native-flash-message";
import Home from './components/src/homeScreen/HomeScreen';
import Auth from './components/src/authScreen/AuthScreen';
import Web from './components/src/webViewScreen/WebView';


const RootStack = createStackNavigator({
  Home: {
    screen: Home,
    navigationOptions: { header: null },
    // headerTintColor: 'royalblue',

  },
  Auth: {
    screen: Auth,
    navigationOptions: { header: null }

  },
  Web: {
    screen: Web,
  
    navigationOptions: { header: null }

  },





},

  {
    initialRouteName: 'Auth',
    // defaultNavigationOptions: {
    //   headerStyle: {
    //     backgroundColor: 'black',
    //   },
      // headerTintColor: '#fff',
      // headerTitleStyle: {
      //   fontWeight: 'bold',
      // },
    // },

  },
  {
    headerMode: 'screen'
  },
);

const AppContainer = createAppContainer(RootStack);

export default class App extends Component {
  render() {
    return (
      <View style={{ flex: 1 }}>
        <StatusBar barStyle="light-content" hidden={false} backgroundColor="black" />
        <AppContainer />
        {/* <FlashMessage position="top" />  */}
        {/* <WebView source={{ uri: 'https://facebook.github.io/react-native/' }} /> */}
      </View>
    );
  }


}




//----------------------------------------------------------------------------//

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});


