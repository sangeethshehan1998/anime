import React, { Component } from 'react';
import {
  View,
  Image,
  NetInfo,
  AsyncStorage,
  TouchableOpacity,
  ImageBackground,
  Alert,
  FlatList,
  TextInput,
  StyleSheet,
  StatusBar,
  Text,
  Linking,
  Button

} from 'react-native';
import { Header, Left, Right, Body, Title } from 'native-base';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Dialog, {
  DialogTitle,
  DialogContent,
  DialogButton,
  ScaleAnimation,
} from 'react-native-popup-dialog';
import InAppBrowser from 'react-native-inappbrowser-reborn';

export default class HomeScreen extends Component {

  constructor(props) {
    super(props);

    this.state = {

      items: [
        { url: 'https://kissanimefree.net/', image: require('../../assets/anima.jpg'), name: "KISSANIMEFREE" },
        { url: 'https://animejo.com/', image: require('../../assets/animasite1.jpg'), name: "ANIMEJO" },
        { url: 'https://www.anime-planet.com/anime/', image: require('../../assets/animasite3.jpg'), name: "ANIME-PLANET" },
        { url: 'https://darkanime.stream/animes/', image: require('../../assets/animasite4.jpg'), name: "DARK-ANIME" },
        { url: 'https://kiss-anime.xyz/', image: require('../../assets/animasite6.jpg'), name: "KISS-ANIME" },
        { url: 'https://www6.kissanime.movie/', image: require('../../assets/animasite5.jpg'), name: "KISSANIME" },

      ],

      spinner: false,
      filteredData: [],
      searchText: "",
      id: '',
      name: '',
      symbol: '',
      price: '',
      percent_change: '',
      oneH: '',
      twentyfourH: '',
      sevenD: '',
      updateTime: '',
      isFetching: false,

    }
  }

  navigateToWeb(item) {
    this.props.navigation.navigate('Web', {
      url: item,
    });
  }

  async openLink(uri) {
    try {
      const url = uri
      if (await InAppBrowser.isAvailable()) {
        const result = await InAppBrowser.open(url, {
          dismissButtonStyle: 'cancel',
          preferredBarTintColor: 'black',
          preferredControlTintColor: 'white',
          readerMode: false,
          animated: true,
          modalPresentationStyle: 'overFullScreen',
          modalTransitionStyle: 'partialCurl',
          modalEnabled: true,
          enableBarCollapsing: false,
          showTitle: true,
          toolbarColor: 'black',
          secondaryToolbarColor: 'black',
          enableUrlBarHiding: true,
          enableDefaultShare: true,
          forceCloseOnRedirection: false,
          animations: {
            startEnter: 'slide_in_right',
            startExit: 'slide_out_left',
            endEnter: 'slide_in_left',
            endExit: 'slide_out_right'
          },
          headers: {
            'my-custom-header': 'my custom header value'
          }
        })
      }
      else Linking.openURL(url)
    } catch (error) {
      Alert.alert(error.message)
    }
  }

  state = {
    defaultAnimationDialog: false,
    scaleAnimationDialog: false,
    slideAnimationDialog: false,
  };
  render() {

    return (

      <View style={{ flex: 1, backgroundColor: 'black' }}>

        <Header noShadow style={{ backgroundColor: 'black' }}>
          <Left>
            <TouchableOpacity onPress={() => {
              this.setState({
                scaleAnimationDialog: true,
              });
            }}>
              <ImageBackground source={require('../../assets/logo3.png')} style={{ height: 40, width: 40, }} >
              </ImageBackground>
            </TouchableOpacity>
          </Left>
          <Body>
          </Body>
        </Header>
        <StatusBar backgroundColor="blak" barStyle="light-content" />

        <FlatList
          onRefresh={() => this.onRefresh()}
          refreshing={this.state.isFetching}
          data={this.state.items}
          renderItem={({ item }) =>
            <TouchableOpacity activeOpacity={0.8} style={{ justifyContent: 'center', alignItems: 'center' }} onPress={() => this.openLink(item.url)}>
              <ImageBackground source={item.image} style={styles.itemContainer} imageStyle={{ borderRadius: 10, }}>
                <View style={{ width: wp('90%'), height: hp('14%'), backgroundColor: 'rgba(155, 0, 0, 0.2)', borderRadius: 10, justifyContent: "center", position: "absolute" }}>
                  <Text style={styles.itemName}>{item.name}</Text>
                </View>
              </ImageBackground>
            </TouchableOpacity>
          }
          ItemSeparatorComponent={this.renderSeparator}
        />
        <Dialog
          onTouchOutside={() => {
            this.setState({ scaleAnimationDialog: false });
          }}
          width={0.9}
          visible={this.state.scaleAnimationDialog}
          dialogAnimation={new ScaleAnimation()}
          onHardwareBackPress={() => {
            console.log('onHardwareBackPress');
            this.setState({ scaleAnimationDialog: false });
            return true;
          }}
          dialogTitle={
            <DialogTitle
              title="About Us"
              hasTitleBar={false}
            />
          }
          actions={[
            <DialogButton
              text="DISMISS"
              onPress={() => {
                this.setState({ scaleAnimationDialog: false });
              }}
              key="button-1"
            />,
          ]}>
          <DialogContent>
            <View>
              <View style={{ justifyContent: "center", alignItems: "center" }}>
                <Image
                  style={{ width: 100, height: 100, }}
                  source={require('../../assets/logo2.png')}
                />
              </View>

              <Text style={{ fontSize: 15, color: '#424242', textAlign: "center", margin: 10 }}>
                Meditation is an application designed to let you watch your watch more relaxing sounds way better with the premium entertainment powered by   "Commercial Technologies Plus".
              </Text>
              <Text style={{ color: 'blue', textAlign: "center" }}
                onPress={() => Linking.openURL('https://commercialtp.com')}>
                Commercial Technologies
                </Text>

            </View>
          </DialogContent>
        </Dialog>

      </View>
    )
  }
}

const styles = StyleSheet.create({
  spinner: {
    color: '#d1b31d'
  },
  itemContainer: {
    borderRadius: 20,
    padding: 10,
    height: hp('14%'),
    width: wp('90%'),
    margin: 5,
    shadowOpacity: 0.8,
    elevation: 7,
    shadowRadius: 30,
    shadowOffset: { width: 1, height: 13 }
  },
  itemName: {
    fontSize: 24,
    color: "white",
    fontWeight: "bold",
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center",
    fontFamily: 'monospace'
  },

})