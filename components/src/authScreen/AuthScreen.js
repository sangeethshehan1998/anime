
import React, { Component } from 'react';
import {
    View,
    Image,
    NetInfo,
    AsyncStorage,
    TouchableOpacity,
    ImageBackground,
    Alert,
    FlatList,
    TextInput,
    StyleSheet,
    StatusBar,
} from 'react-native';
import BackgroundTimer from 'react-native-background-timer';
import Spinner from "react-native-spinkit";
import { NavigationActions, StackActions } from 'react-navigation'

export default class AuthScreen extends Component {

    state = {
        iconPath: "",
        password: '',
        merchantData: null,
        spinner: false,
        text: '',
        data: [],
        id: "",
        color: "#FFFFFF",
        isVisible: true,
        isLoading: true,
        size: 75,
        color: "#FFFFFF",
        isVisible: true,

    };

    componentDidMount() {
        this.test()

    }

    test() {

        const timeoutId = BackgroundTimer.setTimeout(() => {
            const resetAction = StackActions.reset({
                index: 0,
                actions: [
                    NavigationActions.navigate({ routeName: 'Home' })],

            });

            this.props.navigation.dispatch(resetAction);

        }, 3000);

    }
    render() {

        return (
            <View style={{ flex: 1, backgroundColor: 'black' }}>
 
                <View style={{
                    position: 'absolute',
                    top: 0, left: 0,
                    right: 0, bottom: 0,
                    justifyContent: 'center',
                    alignItems: 'center'
                }}>
                    <Image
                        source={require("../../assets/logo3.png")}
                        style={{ width: 100, height: 120 }}
                    ></Image>
                    <Spinner style={styles.spinner}
                        isVisible={this.state.isVisible}
                        size={this.state.size} type={"ThreeBounce"}
                        color={this.state.color} />
                </View>

            </View>
        )
    }
}


const styles = StyleSheet.create({
    spinner: {
        color: '#7B4299',
        top: 30
    },

})