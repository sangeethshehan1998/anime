
import React, { Component } from 'react';
import {
    StyleSheet,

} from 'react-native';
import { WebView } from 'react-native-webview';

export default class WebViewScreen extends Component {


    constructor(props) {
        super(props);

        this.state = {

            url: ''


        }
        this.state.url = this.props.navigation.state.params.url
    }
    render() {

        return (
            <WebView source={{ uri: this.state.url }} style={{ backgroundColor: 'black' }} />
        )
    }
}


const styles = StyleSheet.create({
    spinner: {
        // marginLeft: 160,
        color: '#d1b31d'
    },

})